package log

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
)

// Uplevel controls the stack frame level for file name, line number
// and function name.  It can be used to embed logging calls in helper
// functions that report the file name, line number and function name of
// the routine that calls the helper.
type Uplevel int

// Up1 is short for Uplevel(1).
var Up1 Uplevel = 1

// Start is used for the entry into a function.
func (lvl Uplevel) Start(context interface{}, function string) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: Started:\n", l.prefix, pid, dt, file, context, funcName)
}

// Startf is used for the entry into a function with a formatted message.
func (lvl Uplevel) Startf(context interface{}, function string, format string, a ...interface{}) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: Started: %s", l.prefix, pid, dt, file, context, funcName, fmt.Sprintf(format, a...))
}

// Complete is used for the exit of a function.
func (lvl Uplevel) Complete(context interface{}, function string) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: Completed:\n", l.prefix, pid, dt, file, context, funcName)
}

// Completef is used for the exit of a function with a formatted message.
func (lvl Uplevel) Completef(context interface{}, function string, format string, a ...interface{}) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: Completed: %s", l.prefix, pid, dt, file, context, funcName, fmt.Sprintf(format, a...))
}

// CompleteErr is used to write an error with complete into the trace.
func (lvl Uplevel) CompleteErr(err error, context interface{}, function string) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: Completed ERROR: %s", l.prefix, pid, dt, file, context, funcName, err)
}

// CompleteErrf is used to write an error with complete into the trace with a formatted message.
func (lvl Uplevel) CompleteErrf(err error, context interface{}, function string, format string, a ...interface{}) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: Completed ERROR: %s: %s", l.prefix, pid, dt, file, context, funcName, fmt.Sprintf(format, a...), err)
}

// Err is used to write an error into the trace.
func (lvl Uplevel) Err(err error, context interface{}, function string) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: ERROR: %s", l.prefix, pid, dt, file, context, funcName, err)
}

// Errf is used to write an error into the trace with a formatted message.
func (lvl Uplevel) Errf(err error, context interface{}, function string, format string, a ...interface{}) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: ERROR: %s: %s", l.prefix, pid, dt, file, context, funcName, fmt.Sprintf(format, a...), err)
}

// ErrFatal is used to write an error into the trace then terminate the program.
func (lvl Uplevel) ErrFatal(err error, context interface{}, function string) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: ERROR: %s", l.prefix, pid, dt, file, context, funcName, err)
	output("%s[%d]: %s: %s: %s: %s: TERMINATING\n", l.prefix, pid, dt, file, context, funcName)
	os.Exit(1)
}

// ErrFatalf is used to write an error into the trace with a formatted message then terminate the program.
func (lvl Uplevel) ErrFatalf(err error, context interface{}, function string, format string, a ...interface{}) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: ERROR: %s: %s", l.prefix, pid, dt, file, context, funcName, fmt.Sprintf(format, a...), err)
	output("%s[%d]: %s: %s: %s: %s: TERMINATING\n", l.prefix, pid, dt, file, context, funcName)
	os.Exit(1)
}

// Infof is used to write information with a formatted message.
func (lvl Uplevel) Infof(context interface{}, function string, format string, a ...interface{}) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: Info: %s", l.prefix, pid, dt, file, context, funcName, fmt.Sprintf(format, a...))
}

// Info is used to write information.
func (lvl Uplevel) Info(context interface{}, function string, msg string) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: Info: %s", l.prefix, pid, dt, file, context, funcName, msg)
}

// Warnf is used to write a warning into the trace with a formatted message.
func (lvl Uplevel) Warnf(context interface{}, function string, format string, a ...interface{}) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: Warning: %s", l.prefix, pid, dt, file, context, funcName, fmt.Sprintf(format, a...))
}

// Queryf is used to write a query into the trace with a formatted message.
func (lvl Uplevel) Queryf(context interface{}, function string, format string, a ...interface{}) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: Query: %s:", l.prefix, pid, dt, file, context, funcName, fmt.Sprintf(format, a...))
}

// DataKV is used to write a key/value pair into the trace.
func (lvl Uplevel) DataKV(context interface{}, function string, key string, value interface{}) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	output("%s[%d]: %s: %s: %s: %s: DATA: %s: %v", l.prefix, pid, dt, file, context, funcName, key, value)
}

// DataBlock is used to write a block of data into the trace.
func (lvl Uplevel) DataBlock(context interface{}, function string, key string, block interface{}) {
	if v, ok := block.(string); ok {
		(lvl + 1).DataString(context, function, v)
		return
	}

	d, err := json.MarshalIndent(block, "", "    ")
	if err != nil {
		d = []byte(err.Error())
	}

	(lvl + 1).DataString(context, function, string(d))
}

// DataString is used to write a string with CRLF each on their own line.
func (lvl Uplevel) DataString(context interface{}, function string, message string) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	lines := bytes.Split([]byte(message), []byte{'\n'})

	var buf bytes.Buffer
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		fmt.Fprintf(&buf, "%s[%d]: %s: %s: %s: %s: DATA: %s\n", l.prefix, pid, dt, file, context, funcName, line)
	}

	output(buf.String())
}

// DataTrace is used to write a block of data from an io.Stringer respecting each line.
func (lvl Uplevel) DataTrace(context interface{}, function string, formatters ...Formatter) {
	dt, file, funcName, pid := dtFile(2+int(lvl), function)
	var lines [][]byte
	for _, f := range formatters {
		lines = append(lines, bytes.Split([]byte(f.Format()), []byte{'\n'})...)
	}

	var buf bytes.Buffer
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		fmt.Fprintf(&buf, "%s[%d]: %s: %s: %s: %s: DATA: %s\n", l.prefix, pid, dt, file, context, funcName, line)
	}

	output(buf.String())
}
