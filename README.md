Logging is an important part of the application and having a consistent logging mechanism and structure is mandatory. With several teams writing different components that talk to each other, being able to read each others logs could be the difference between finding bugs quickly or wasting hours.

Project page: https://github.comcast.com/vbo/go-log/log  
Package docs: http://godoc.org/github.comcast.com/vbo/vbo/go-log/log
