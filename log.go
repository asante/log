package log

import (
	"fmt"
	"io"
	"os"
	"path"
	"runtime"
	"sync"
	"time"
)

// Date and time layout for each trace line.
const layout = "2006/01/02 15:04:05.000"

// Formatter provide support for special formatting.
type Formatter interface {
	Format() string
}

// logger maintains internal state for our logger.
type logger struct {
	dest   io.Writer
	mu     sync.Mutex // ensures atomic writes.
	prefix string
	test   bool
}

// logger maintains a pointer to the single logger.
var l = logger{
	dest:   os.Stdout,
	prefix: "PREFIX",
}

// Init initializes the logging system for use.
func Init(prefix string, w ...io.Writer) {
	l.prefix = prefix

	if len(w) == 0 {
		w = append(w, os.Stdout)
	}

	if len(w) > 1 {
		l.dest = io.MultiWriter(w...)
		return
	}

	l.dest = w[0]
}

// InitTest configures the logger for testing purposes.
func InitTest(prefix string, w ...io.Writer) {
	l.test = true

	Init(prefix, w...)
}

// dtFile returns the current time and file for logging.
func dtFile(calldepth int, function string) (dateTime string, file string, funcName string, pid int) {
	// Capture the name of the function logging if
	// a function was not provided.
	if function == "" {
		pc := make([]uintptr, calldepth+1)
		runtime.Callers(calldepth, pc)
		f := runtime.FuncForPC(pc[calldepth-1])
		_, funcName = path.Split(f.Name())
	} else {
		funcName = function
	}

	if l.test {
		return time.Date(2009, time.November, 10, 15, 0, 0, 0, time.UTC).UTC().Format(layout), "file.go:512", funcName, 69910
	}

	dateTime = time.Now().UTC().Format(layout)

	_, filePath, line, ok := runtime.Caller(calldepth)
	if !ok {
		return dateTime, "unknown.go:0:", "missing", os.Getpid()
	}
	_, file = path.Split(filePath)

	return dateTime, fmt.Sprintf("%s:%d", file, line), funcName, os.Getpid()
}

// output performs the actual write to the destination device.
func output(format string, a ...interface{}) {
	s := fmt.Sprintf(format, a...)

	if s[len(s)-1] == '\n' {
		l.mu.Lock()
		{
			l.dest.Write([]byte(s))
		}
		l.mu.Unlock()
		return
	}

	l.mu.Lock()
	{
		fmt.Fprintf(l.dest, "%s\n", s)
	}
	l.mu.Unlock()
}
